﻿using System.Data.Entity;
using Nasa.Ksc.LevelUp;
using Nasa.Ksc.LevelUp.Models;
using System.Data.Entity.Infrastructure;

namespace LevelUp.Models
{
    public class LevelUpContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<LevelUp.Models.LevelUpContext>());

        public LevelUpContext() : base("name=LevelUpContext")
        {
        }

        public DbSet<Award> Awards { get; set; }
        public DbSet<Trophy> Trophies {get;set;}
        public DbSet<Achievement> Achievements {get;set;}
        public DbSet<SimpleCondition> Conditions { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Nasa.Ksc.LevelUp.Models.System> Systems { get; set; }
        public DbSet<User> Users { get; set; }
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region Model Definitions
                // Table-per-Hierarchy for all award types
                modelBuilder.Entity<AwardBase>()
                    .Map<Award>(m => m.Requires("Type").HasValue("Award"))
                    .Map<Trophy>(m => m.Requires("Type").HasValue("Trophy"))
                    .Map<Achievement>(m => m.Requires("Type").HasValue("Achievement"))
                    .HasKey(a => a.Id)
                    .ToTable("Awards");

                modelBuilder.Entity<ConditionBase>()
                    .Map<SimpleCondition>(c => c.Requires("Type").HasValue("Simple"))
                    .Map<ComplexCondition>(c => c.Requires("Type").HasValue("Complex"))
                    .HasKey(c => c.Id)
                    .ToTable("Conditions");

                // Other boring model definitions
                modelBuilder.Entity<Nasa.Ksc.LevelUp.Models.System>()
                    .HasKey(s => s.Id)
                    .ToTable("System");

                modelBuilder.Entity<User>()
                    .HasKey(u => u.Id)
                    .ToTable("Users");

                modelBuilder.Entity<Group>()
                    .HasKey(g => g.Id)
                    .ToTable("Groups");

                modelBuilder.Entity<SimpleCondition>()
                    .HasKey(c => c.Id)
                    .ToTable("Conditions");

                // Composite entity keys
                modelBuilder.Entity<GroupAward>()
                    .HasKey(g => new { g.AwardId, g.GroupId })
                    .ToTable("GroupAwards");

                modelBuilder.Entity<UserAward>()
                    .HasKey(u => new { u.AwardId, u.UserId })
                    .ToTable("UserAwards");

                modelBuilder.Entity<GroupUser>()
                    .HasKey(g => new { g.GroupId, g.UserId })
                    .ToTable("GroupUsers");
            #endregion

            #region Relational Mappings
                modelBuilder.Entity<Group>()
                    .HasMany<Award>(g => g.Awards);
                modelBuilder.Entity<Group>()
                    .HasMany<Trophy>(g => g.Trophies);
                modelBuilder.Entity<Group>()
                    .HasMany<Achievement>(g => g.Achievements);
                modelBuilder.Entity<Group>()
                    .HasMany<User>(g => g.Users);

                modelBuilder.Entity<User>()
                    .HasMany<Award>(a => a.Awards);
                modelBuilder.Entity<User>()
                    .HasMany<Group>(u => u.Groups);
            #endregion
        }
    }
}
