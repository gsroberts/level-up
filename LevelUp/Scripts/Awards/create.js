$(document).ready(function(){
	$("div.selector-item").on("click", function(){
		var $display = $("#award-display");
	
		var selectedClass = $(this).parent().data("selector-type");
		var selectedItem = $(this).data("selected-item");
		
		var bgClass = $("#background-class").val();
		var fgClass = $("#foreground-class").val();
		
		if(selectedClass === "bg")
		{
			$display.children(":first")
				.removeClass(bgClass)
				.addClass(selectedItem);
			
			$("#background-class").val(selectedItem);
		}
		else if(selectedClass === "fg")
		{
			$display.children(":first")
				.removeClass(fgClass)
				.addClass(selectedItem);
			
			$("#foreground-class").val(selectedItem);
		}
		
		$(this).parents(".selector-element").children(".selector-item").css({"border": "none"});
		$(this).css({"border": "1px solid black"});
	});
});