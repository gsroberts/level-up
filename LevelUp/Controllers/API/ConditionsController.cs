﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Nasa.Ksc.LevelUp;
using Nasa.Ksc.LevelUp.Models;

namespace LevelUp.Controllers.API
{
    public class ConditionsController : ApiController
    {
        // GET api/condition
        public IEnumerable<SimpleCondition> Get()
        {
            return new List<SimpleCondition>() {                
                new SimpleCondition( "Commits", ConditionOperator.GreaterThanOrEqualTo, 1, 10 ),
                new SimpleCondition( "BuildOperations", ConditionOperator.GreaterThanOrEqualTo, 2, 50 )
            };
        }

        // GET api/condition/5
        public SimpleCondition Get(int id)
        {
            SimpleCondition condition = new SimpleCondition("NumberOfReviews", ConditionOperator.GreaterThanOrEqualTo, 1, 10);
            return condition;
        }

        // POST api/condition
        public void Post([FromBody]string value)
        {
        }

        // PUT api/condition/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/condition/5
        public void Delete(int id)
        {
        }
    }
}
