﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Nasa.Ksc.LevelUp;
using Nasa.Ksc.LevelUp.Models;

namespace LevelUp.Controllers.API
{
    public class UsersController : ApiController
    {
        LevelUp.Models.LevelUpContext currentContext = new Models.LevelUpContext();

        // GET api/user
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/user/5
        [HttpGet]
        public UserState GetUserState(int id)
        {
            UserState userState = new UserState();           

            userState.AddAward(new Award(1, "Professional Source Controller", 1, new List<ConditionBase>() { 
                new SimpleCondition("NumberOfCheckins", ConditionOperator.GreaterThanOrEqualTo, 1, 50)
            }), this.Get(id));
            userState.AddAward(new Achievement(70, "Broken Build Debugger", 1, new List<ConditionBase>() { 
                new SimpleCondition("ConsecutiveSuccessfulBuilds", ConditionOperator.GreaterThanOrEqualTo, 1, 10),
                new SimpleCondition("LastBrokenBuild", ConditionOperator.GreaterThanOrEqualTo, 1, 10)
            }), this.Get(id));
            userState.AddAward(new Trophy(140, "Journeyman Architect", 1, new List<ConditionBase>() { 
                new SimpleCondition("PercentCodeChurn", ConditionOperator.LessThanOrEqualTo, 1, .5),
                new SimpleCondition("ReducedComplexityFromPreviousBuild", ConditionOperator.Equals, 1, true),
                new SimpleCondition("NumberOfRevertOperations", ConditionOperator.LessThan, 1, 1)
            }), this.Get(id));

            return userState;
        }

        [HttpGet]
        public User Get(int id)
        {
            return currentContext.Users.Where(u => u.Id == id).FirstOrDefault();
        }

        // POST api/user
        public void Post([FromBody]string value)
        {
        }

        // PUT api/user/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/user/5
        public void Delete(int id)
        {
        }
    }
}
