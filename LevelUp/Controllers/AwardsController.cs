﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Nasa.Ksc.LevelUp;
using Nasa.Ksc.LevelUp.Models;
using LevelUp.Models;

namespace LevelUp.Controllers
{
    public class AwardsController : Controller
    {
        private LevelUpContext db = new LevelUpContext();

        //
        // GET: /Default1/

        public ActionResult Index()
        {
            return View(db.Awards.ToList());
        }

        //
        // GET: /Default1/Details/5

        public ActionResult Details(int id = 0)
        {
            AwardBase awardbase = db.Awards.Find(id);
            if (awardbase == null)
            {
                return HttpNotFound();
            }
            return View(awardbase);
        }

        //
        // GET: /Default1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Default1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AwardBase awardbase)
        {
            if (ModelState.IsValid)
            {
                db.Awards.Add((Award)awardbase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(awardbase);
        }

        //
        // GET: /Default1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AwardBase awardbase = db.Awards.Find(id);
            if (awardbase == null)
            {
                return HttpNotFound();
            }
            return View(awardbase);
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AwardBase awardbase)
        {
            if (ModelState.IsValid)
            {
                db.Entry(awardbase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(awardbase);
        }

        //
        // GET: /Default1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            AwardBase awardbase = db.Awards.Find(id);
            if (awardbase == null)
            {
                return HttpNotFound();
            }
            return View(awardbase);
        }

        //
        // POST: /Default1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AwardBase awardbase = db.Awards.Find(id);
            db.Awards.Remove((Award)awardbase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}