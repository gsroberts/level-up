﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nasa.Ksc.LevelUp.Models;

namespace Nasa.Ksc.LevelUp
{
    public interface IParticipant
    {
        int Id { get; set; }

        ICollection<Award> Awards { get; set; }
        ICollection<Trophy> Trophies { get; set; }
        ICollection<Achievement> Achievements { get; set; }
    }
}
