﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp.Models
{
    public class GroupAward
    {
        public int GroupId { get; set; }
        public int AwardId { get; set; }
        public bool Unlocked { get; set; }
        public bool HasMultiple { get; set; }
        public int AwardMultiplier { get; set; }

        public GroupAward()
        {
        }
    }
}
