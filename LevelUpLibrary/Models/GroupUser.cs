﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp.Models
{
    public class GroupUser
    {
        public int GroupId { get; set; }
        public int UserId { get; set; }
    }
}
