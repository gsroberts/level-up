﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp.Models
{
    public class Award : AwardBase
    {
        public Award()
        {
        }

        public Award(int id, string name, int system, List<ConditionBase> conditions) :
            base(id, name, system, conditions)
        {
        }

    }
}
