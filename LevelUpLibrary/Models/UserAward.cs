﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp.Models
{
    public class UserAward
    {
        public int UserId { get; set; }
        public int AwardId { get; set; }
        public bool Unlocked { get; set; }
        public bool HasMultiple { get; set; }
        public int AwardMultiplier { get; set; }

        public UserAward() { }

        public UserAward(User user, IAward award)
        {
            this.UserId= user.Id;
            this.AwardId = award.Id;
            this.Unlocked = false;
            this.HasMultiple = false;
            this.AwardMultiplier = 1;
        }
    }
}
