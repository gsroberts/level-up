﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp.Models
{
    
    public class User : Participant
    {
        public string Uupic { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ICollection<Group> Groups { get; set; }

        public int CumulativeAwards
        {
            get
            {
                int total = 0;

                total += this.Awards.Count;
                total += this.Achievements.Count;
                total += this.Trophies.Count;

                return total;
            }
        }

        public User()
        {
        }

        public User(int id, string uupic, string firstName, string lastName)
        {
            this.Id = id;
            this.Uupic = uupic;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        private int GetNumberOfAwardsCompleteForCollection(IEnumerable<UserAward> targetCollection)
        {
            int total = 0;

            foreach (UserAward award in targetCollection)
            {
                if (award.Unlocked)
                {
                    if (award.HasMultiple)
                    {
                        total += award.AwardMultiplier;
                    }
                    else
                    {
                        total += 1;
                    }
                }
            }

            return total;
        }
    }
}
