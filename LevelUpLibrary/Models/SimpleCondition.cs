﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Reflection;

namespace Nasa.Ksc.LevelUp.Models
{
    public class SimpleCondition : ConditionBase
    {
        private ConditionOperator oper;

        public object Attribute { get; set; }
        public object Value { get; set; }
        public string Operator 
        {
            get
            {
                return oper.GetStringValue();
            }
            set
            {
                Type operEnum = typeof(ConditionOperator);

                global::System.Reflection.FieldInfo[] fields = operEnum.GetFields();

                int numFields = fields.Count();

                ConditionOperator op = ConditionOperator.None;

                for (int i = 0; i < numFields; ++i)
                {
                    global::System.Reflection.FieldInfo field = fields[i];

                    EnumStringValueAttribute[] custAttr = (EnumStringValueAttribute[])field.GetCustomAttributes(typeof(EnumStringValueAttribute), false);

                    if (custAttr.Length > 0 && custAttr[0].Value == value)
                    {
                        op = (ConditionOperator)field.GetValue(null);
                    }
                }

                this.oper = op;
            }
        }

        public override bool IsSet
        {
            get
            {
                return false;
            }
        }

        public SimpleCondition(string attribute, ConditionOperator oper, int system, object value, int id, bool isSet)
            :base(id, system, isSet)
        {
            this.Attribute = attribute;
            this.oper = oper;
            this.SystemId = system;
            this.Value = value;
        }

        public SimpleCondition()
        {
        }
    }
}
