﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace Nasa.Ksc.LevelUp
{
    public enum ConditionOperator
    {
        [EnumStringValue("None")]
        None = 0,
        [EnumStringValue("=")]
        Equals = 1,
        [EnumStringValue(">")]
        GreaterThan = 2,
        [EnumStringValue("<")]
        LessThan = 3,
        [EnumStringValue(">=")]
        GreaterThanOrEqualTo = 4,
        [EnumStringValue("<=")]
        LessThanOrEqualTo = 5,
        [EnumStringValue("!=")]
        NotEqualTo = 6
    }
}
