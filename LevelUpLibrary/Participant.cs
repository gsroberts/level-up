﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nasa.Ksc.LevelUp.Models;

namespace Nasa.Ksc.LevelUp
{
    public class Participant : IParticipant
    {
        public int Id { get; set; }

        public virtual ICollection<Award> Awards { get; set; }
        public virtual ICollection<Trophy> Trophies { get; set; }
        public virtual ICollection<Achievement> Achievements { get; set; }
    }
}
