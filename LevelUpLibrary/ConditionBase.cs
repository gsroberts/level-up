﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Nasa.Ksc.LevelUp
{
    public class ConditionBase : ICondition
    {
        public int Id { get; set; }

        public int SystemId {get;set;}

        public string Name { get; set; }

        public virtual bool IsSet { get { return false; } }

        public ConditionBase(int id, int system, bool IsSet)
        {
            this.Id = id; 
            this.SystemId = system;
        }

        public ConditionBase()
        {
        }
    }
}
