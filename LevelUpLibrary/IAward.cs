﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp
{
    public interface IAward
    {
        int Id { get; set; }
        string Name { get; set; }
        int System { get; set; }
        string Description { get; set; }
        string BackgroundImage { get; set; }
        string ForegroundImage { get; set; }
        string Type { get; set; }

        ICollection<ConditionBase> Conditions { get; set; }
    }
}
