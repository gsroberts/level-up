﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp
{
    public class EnumStringValueAttribute : System.Attribute
    {
        private string enumStringValue;

        public EnumStringValueAttribute(string value)
        {
            this.enumStringValue = value;
        }

        public string Value
        {
            get
            {
                return this.enumStringValue;
            }
        }
    }
}
