﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp
{
    public class AwardBase : IAward
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int System { get; set; }
        public string Description { get; set; }
        public string BackgroundImage { get; set; }
        public string ForegroundImage { get; set; }
        public string Type { get; set; }

        public ICollection<ConditionBase> Conditions { get; set; }

        public AwardBase()
        {
        }

        public AwardBase(int id, string name, int system, List<ConditionBase> conditions)
        {
            this.Id = id;
            this.Name = name;
            this.System = system;
            this.Conditions = conditions;
        }

    }
}
