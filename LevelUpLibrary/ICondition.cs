﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nasa.Ksc.LevelUp
{
    public interface ICondition
    {
        int Id { get; set; }
        int SystemId { get; set; }
        bool IsSet { get; }
    }
}
