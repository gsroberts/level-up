﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nasa.Ksc.LevelUp.Models;

namespace Nasa.Ksc.LevelUp
{
    /// <summary>
    /// Represents the awards a user has or has not been awarded
    /// </summary>
    public class UserState
    {
        public int Id { get; set; }
        public List<UserAward> Awards { get; set; }
        public List<UserAward> Trophies { get; set; }
        public List<UserAward> Achievements { get; set; }

        public int CumulativeAwards 
        {
            get
            {
                int total = 0;

                total += this.GetNumberOfAwardsCompleteForCollection(this.Awards);
                total += this.GetNumberOfAwardsCompleteForCollection(this.Achievements);
                total += this.GetNumberOfAwardsCompleteForCollection(this.Trophies);

                return total;
            }
        }

        public UserState()
        {
            this.Achievements = new List<UserAward>();
            this.Awards = new List<UserAward>();
            this.Trophies = new List<UserAward>();
        }

        /// <summary>
        /// Adds an IAward to the "target" collection
        /// </summary>
        /// <param name="award">IAward object</param>
        /// <returns>True if operation succeeds, false if failure</returns>
        public bool AddAward(IAward award, User user)
        {
            try
            {
                Type awardType = award.GetType();

                UserAward userAward = new UserAward(user, award);

                switch (awardType.Name)
                {
                    case "Award":
                        this.Awards.Add(userAward);
                        break;
                    case "Trophy":
                        this.Trophies.Add(userAward);
                        break;
                    case "Achievement":
                        this.Achievements.Add(userAward);
                        break;
                    default:
                        return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private int GetNumberOfAwardsCompleteForCollection(IEnumerable<UserAward> targetCollection)
        {
            int total = 0;

            foreach (UserAward award in targetCollection)
            {
                if (award.Unlocked)
                {
                    if (award.HasMultiple)
                    {
                        total += award.AwardMultiplier;
                    }
                    else
                    {
                        total += 1;
                    }
                }
            }

            return total;
        }
    }
}
