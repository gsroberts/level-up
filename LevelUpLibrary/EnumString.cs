﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Nasa.Ksc.LevelUp
{
    public static class EnumString
    {
        public static string GetStringValue(this Enum value)
        {
            string stringValue = null;
            Type type = value.GetType();

            FieldInfo fi = type.GetField(value.ToString());
            EnumStringValueAttribute[] attrs = (EnumStringValueAttribute[])fi.GetCustomAttributes(typeof(EnumStringValueAttribute), false);

            if (attrs.Length > 0)
            {
                stringValue = attrs[0].Value;
            }

            return stringValue;
        }
    }
}
